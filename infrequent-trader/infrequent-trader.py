import datetime as dt
import collections
import math
import numpy as np
import pandas as pd

from quantopian.algorithm import attach_pipeline, pipeline_output
from quantopian.pipeline import CustomFactor, Pipeline
from quantopian.pipeline.data import morningstar
from zipline.api import get_environment
from zipline.finance.commission import PerShare

###----------------------------------------
### Global Constants

initial_leverage = 0.95
# initial_short_ratio is 1 - initial_long_ratio
initial_long_ratio = 0.67

###----------------------------------------
### Utility Functions

### Logging

def stocks_to_symbols(stocks):
    return [s.symbol for s in stocks]

def log_portfolio_allocation(prefix, context, data):
    alloc = allocations_from_portfolio(context.portfolio, data, context.all_prices,
                                       context.account.leverage)
    stmt = {}
    for [s, alloc] in alloc.stock_allocations.iteritems():
        stmt[s.symbol] = alloc
    log.info("{}: {}".format(prefix, stmt))

def log_positions_and_sizes(prefix, positions):
    pns = {}
    for stock in positions:
        pns[stock.symbol] = positions[stock].amount
    log.info(prefix + ' ' + ' Positions: ' + str(pns))

def stocks_map_to_symbols_map(m):
    return [[s.symbol, val] for [s, val] in m.iteritems()] if m else {}

###----------------------------------------
### Base Classes

class PortfolioState(object):
    '''
    Stocks and the associated proportion of the portfolio they
    have. Can be used to describe the current or the future state.
    Given a data object can provide a map of stocks to amounts
    based on current prices.
    '''
    def __init__(self, stock_allocations, long_ratio, short_ratio, leverage, value):
        self.stock_allocations = stock_allocations
        self.long_ratio = long_ratio
        self.short_ratio = short_ratio
        self.leverage = leverage
        self.value = value

    @staticmethod
    def normalize_allocation(alloc, long_ratio, short_ratio, leverage, value, caller):
        normalized = collections.OrderedDict()
        short_total = 0.0
        long_total = 0.0
        target_long = long_ratio*leverage
        target_short = -short_ratio*leverage
        for [stock, weight] in alloc.iteritems():
            if math.isnan(weight):
                raise Exception('nan stock weight {} {}'.format(stock.symbol, weight))
            if weight < 0.0:
                short_total += weight
            else:
                long_total += weight
        if (short_total == 0.0 and target_short < 0.0) or math.isnan(short_total):
            raise Exception('{}: Invalid short total {}'.format(caller, short_total))
        if (long_total == 0.0 and target_long < 0.0) or math.isnan(long_total):
            raise Exception('{}: Invalid long total {}'.format(caller, long_total))
        for [stock, weight] in alloc.iteritems():
            if weight < 0.0:
                normalized[stock] = weight*target_short/short_total
            else:
                normalized[stock] = weight*target_long/long_total
        return PortfolioState(normalized, long_ratio, short_ratio, leverage, value)

    def change_leverage(self, new_leverage, caller):
        return PortfolioState.normalize_allocation(
            self.stock_allocations, self.long_ratio, self.short_ratio,
            new_leverage, self.value, caller)

class StockTransactions(object):
    '''
    Computes the difference between current and target portfolios.
    '''
    def __init__(self, buy_orders, sell_orders, long_ratio, short_ratio, leverage):
        self.buy_orders = buy_orders
        self.sell_orders = sell_orders
        self.long_ratio = long_ratio
        self.short_ratio = short_ratio
        self.leverage = leverage

class StockSelectionAlgorithm(object):
    '''
    Abstract class to select stocks and weights from given state.
    Produces a portfolio allocation.
    '''
    def __init__(self):
        self._longs_target = 0
        self._shorts_target = 0
        self._longs_universe = []
        self._longs_universe = []
        self._portfolio = None
        self._selection_date = None

    @property
    def longs_target(self):
        return self._longs_target

    @property
    def shorts_target(self):
        return self._shorts_target

    @property
    def longs_universe(self):
        return self._longs_universe

    @property
    def shorts_universe(self):
        return self._longs_universe

    @property
    def portfolio(self):
        return self._portfolio

    @property
    def selection_date(self):
        return self._selection_date

    def before_trading_start(self, context, data):
        pass

    def select_stocks(self, context, data):
        pass

    def is_select_needed(self, context, data):
        return False

    def select_stocks_if_needed(self, context, data):
        if self.is_select_needed(context, data):
            result = self.select_stocks(context, data)
            longs = 0
            shorts = 0
            for alloc in result.stock_allocations.values():
                if alloc > 0:
                    longs += 1
                else:
                    shorts += 1
            log.info('Selected {} longs, {} shorts.'.format(longs, shorts))
            self._portfolio = result
            self._selection_date = get_datetime()
            context.base_portfolio = result
            context.target_portfolio = None

class StockAllocationAdjustment(object):
    '''
    Decorator that modifies stocks and weights.
    '''
    def __init__(self):
        self._adjust_date = None

    @property
    def adjust_date(self):
        return self._adjust_date

    @property
    def extra_stocks(self):
        return []

    def before_trading_start(self, context, data):
        pass

    def adjust_allocation(self, allocation, context, data):
        return PortfolioState.normalize_allocation(
            allocation.stock_allocations, allocation.long_ratio, allocation.short_ratio,
            allocation.leverage, allocation.value, 'StockAllocationAdjustment.adjust_allocation')

    def unadjust_allocation(self, allocation, context):
        return PortfolioState.normalize_allocation(
            allocation.stock_allocations, allocation.long_ratio, allocation.short_ratio,
            allocation.leverage, allocation.value, 'StockAllocationAdjustment.unadjust_allocation')

    def is_adjust_needed(self, base, context, data):
        return False

    def refill_stock_allocations(self, allocs, context):
        for [s, w] in context.selector.portfolio.stock_allocations.iteritems():
            if not s in allocs and context.all_can_trade.get(s, False):
                allocs[s] = w

    def adjust_stocks_if_needed(self, context, data):
        if ((context.base_portfolio and not context.target_portfolio) or
            self.is_adjust_needed(context.base_portfolio, context, data)):
            if not context.base_portfolio:
                current = allocations_from_portfolio(
                    context.portfolio, data, context.all_prices, context.account.leverage)
                context.base_portfolio = self.unadjust_allocation(current, context).change_leverage(
                    context.target_leverage, "adjust_stocks_if_needed")
            context.target_portfolio = self.adjust_allocation(context.base_portfolio, context, data)
            self._adjust_date = get_datetime()

    def record_stats(self, context, data):
        longs = 0.0
        shorts = 0.0
        all_prices = context.all_prices
        if all_prices is None:
            all_prices = data.current(context.portfolio.positions.keys(), 'price')
        for [s, p] in context.portfolio.positions.iteritems():
            price = all_prices[s]
            if p.amount > 0:
                longs += price*p.amount
            else:
                shorts += price*p.amount
        if context.base_portfolio:
            record(leverage=context.account.leverage,
                   real_short_ratio=-shorts/(context.portfolio.portfolio_value*context.target_leverage),
                   real_long_ratio=longs/(context.portfolio.portfolio_value*context.target_leverage),
                   target_short_ratio=context.base_portfolio.short_ratio,
                   target_long_ratio=context.base_portfolio.long_ratio)
        else:
            record(leverage=context.account.leverage,
                   real_short_ratio=-shorts/(context.portfolio.portfolio_value*context.target_leverage),
                   real_long_ratio=longs/(context.portfolio.portfolio_value*context.target_leverage))

class SequentialStockAllocationAdjustment(StockAllocationAdjustment):

    def __init__(self, adjustments):
        StockAllocationAdjustment.__init__(self)
        self.adjustments = adjustments

    @property
    def extra_stocks(self):
        result = set()
        for adj in self.adjustments:
            result.union(adj.extra_stocks)
        return result

    def before_trading_start(self, context, data):
        for adj in self.adjustments:
            adj.before_trading_start(context, data)

    def adjust_allocation(self, allocation, context, data):
        new_portfolio = allocation
        for adj in self.adjustments:
            new_portfolio = adj.adjust_allocation(new_portfolio, context, data)
        return new_portfolio

    def unadjust_allocation(self, allocation, context):
        new_portfolio = allocation
        for adj in reversed(self.adjustments):
            new_portfolio = adj.unadjust_allocation(new_portfolio, context)
        return new_portfolio

    def is_adjust_needed(self, base, context, data):
        for adj in self.adjustments:
            if not adj.is_adjust_needed(base, context, data):
                return False
        return True

    def record_stats(self, context, data):
        self.adjustments[0].record_stats(context, data)

class StockTransactionExecutor(object):
    '''
    Executes stock transactions.
    '''

    min_holding_percent = 1e-5

    @staticmethod
    def should_trade(stock, curr_alloc, tgt_alloc, all_prices, all_can_trade, portfolio_value):
        curr_price = all_prices[stock]
        value_diff = abs(curr_alloc - tgt_alloc)*portfolio_value
        return (all_can_trade.get(stock, False) and
                ((value_diff > 2*curr_price and (abs(curr_alloc - tgt_alloc) > tgt_alloc*0.01)) or
                 (abs(tgt_alloc) <= StockTransactionExecutor.min_holding_percent and value_diff > 0.0)))

    def execute_sell_transactions(self, sell_orders, current_allocations, all_can_trade,
                                  all_prices, portfolio_value):
        min_holding_percent = StockTransactionExecutor.min_holding_percent
        should_trade = StockTransactionExecutor.should_trade
        new_sell_orders = sell_orders.copy()
        for [s, tgt_alloc] in sell_orders.iteritems():
            curr_alloc = current_allocations.stock_allocations.get(s, 0.0)
            if should_trade(s, curr_alloc, tgt_alloc, all_prices, all_can_trade, portfolio_value):
                order_target_percent(s, 0.0 if abs(tgt_alloc) < min_holding_percent else tgt_alloc)
            else:
                del new_sell_orders[s]
        if not new_sell_orders:
            log.info('All confirmed sold')
        return new_sell_orders

    def execute_buy_transactions(self, buy_orders, current_allocations, all_can_trade,
                                 all_prices, portfolio_value, excess_cash):
        min_holding_percent = StockTransactionExecutor.min_holding_percent
        should_trade = StockTransactionExecutor.should_trade
        new_buy_orders = buy_orders.copy()
        for [s, tgt_alloc] in buy_orders.iteritems():
            curr_alloc = current_allocations.stock_allocations.get(s, 0.0)
            if should_trade(s, curr_alloc, tgt_alloc, all_prices, all_can_trade, portfolio_value):
                if excess_cash > 0:
                    alloc = 0.0 if abs(tgt_alloc) < min_holding_percent else tgt_alloc
                    order_target_percent(s, alloc)
            else:
                del new_buy_orders[s]
        if not new_buy_orders:
            log.info('All confirmed bought')
        return new_buy_orders

    def execute_transactions(self, context, data, current, transactions):
        portfolio = context.portfolio
        target_leverage = transactions.leverage
        all_prices = context.all_prices
        all_can_trade = context.all_can_trade
        portfolio_value = portfolio.portfolio_value
        target_cash_ratio = 1 - target_leverage*transactions.long_ratio
        excess_cash = portfolio.cash - portfolio_value*target_cash_ratio
        log.info('Excess cash {} portfolio.cash {} portfolio_value {} target_cash_ratio {} target_leverage {} transactions.long_ratio {}'.format(
            excess_cash, portfolio.cash, portfolio_value, target_cash_ratio, target_leverage, transactions.long_ratio))
        if transactions.buy_orders or transactions.sell_orders:
            sell_orders = transactions.sell_orders
            buy_orders = transactions.buy_orders
            # We aren't going to get any more money now
            if not sell_orders and excess_cash < 0:
                return None
            new_sells = (self.execute_sell_transactions(sell_orders, current, all_can_trade,
                                                        all_prices, portfolio_value)
                         if sell_orders else None)
            new_buys = (self.execute_buy_transactions(buy_orders, current, all_can_trade,
                                                      all_prices, portfolio_value, excess_cash)
                        if buy_orders else None)
            if new_sells or new_buys:
                return StockTransactions(new_buys, new_sells, transactions.long_ratio,
                                         transactions.short_ratio, transactions.leverage)
            else:
                return None
        else:
            return None

###----------------------------------------
### Implementation Of Base Classes

### Quantitative Value Algorithm

class TrivialStockSelector(StockSelectionAlgorithm):

    def __init__(self, portfolio_value):
        StockSelectionAlgorithm.__init__(self)

    def select_stocks(self, context, data):
        lr = initial_long_ratio
        sr = 1 - lr
        alloc = {symbol('AAPL'): context.target_leverage*lr,
                 symbol('IBM'): -context.target_leverage*sr}
        return PortfolioState(alloc, lr, sr, context.target_leverage,
                              context.portfolio.portfolio_value)

    def is_select_needed(self, context, data):
        today = get_datetime()
        return ((not self.selection_date) or
                ((today - self.selection_date).days > 365) or
                (context.account.leverage > context.target_leverage*1.15))

###----------------------------------------
### Initialize

def set_context_parameters(context):
    # Proportion of the portfolio value we want as cash
    context.target_leverage = initial_leverage
    context.base_portfolio = None
    context.target_portfolio = None
    context.last_transactions = None

def compute_transactions(context, current):
    target = context.target_portfolio
    all_prices = context.all_prices
    all_can_trade = context.all_can_trade
    portfolio_value = context.portfolio.portfolio_value
    last_sell_orders = context.last_transactions and (context.last_transactions.sell_orders or {})
    last_buy_orders = context.last_transactions and (context.last_transactions.buy_orders or {})
    # We don't need to work on cash directly, it will
    # be taken care of as a side effect of buys/sells
    sell_orders = collections.OrderedDict()
    buy_orders = collections.OrderedDict()
    # Sell everything in current but not in target
    for [s, alloc] in current.stock_allocations.iteritems():
        if all_can_trade.get(s, False) and s not in target.stock_allocations:
            if alloc >= 0.0 and (last_sell_orders is None or s in last_sell_orders):
                sell_orders[s] = 0.0
            elif alloc < 0.0 and (last_buy_orders is None or s in last_buy_orders):
                buy_orders[s] = 0.0
    # Buy everything in target not in current
    # Adjust everything in target and also in current
    for [s, ta] in target.stock_allocations.iteritems():
        ca = current.stock_allocations.get(s, 0.0)
        if StockTransactionExecutor.should_trade(s, ca, ta, all_prices, all_can_trade, portfolio_value):
            if ta > ca and (last_buy_orders is None or s in last_buy_orders):
                buy_orders[s] = ta
            elif ta < ca and (last_sell_orders is None or s in last_sell_orders):
                sell_orders[s] = ta
    return StockTransactions(buy_orders or None, sell_orders or None,
                             target.long_ratio, target.short_ratio, target.leverage)

def allocations_from_portfolio(portfolio, data, all_prices, leverage):
    stocks = collections.OrderedDict()
    pv = portfolio.portfolio_value
    long_total = 0.0
    short_total = 0.0
    for [s, p] in portfolio.positions.iteritems():
        # If we can't get a price we leave it out of allocations
        if s not in all_prices:
            continue
        value = all_prices[s]*p.amount
        if math.isnan(value):
            value = 0
        if value < 0:
            short_total += value
        else:
            long_total += value
        alloc = value/pv
        stocks[s] = alloc
    short_total = -short_total
    total = long_total + short_total
    long_ratio = 0 if long_total == 0.0 else long_total/total
    short_ratio = 0 if short_total == 0.0 else short_total/total
    return PortfolioState.normalize_allocation(
        stocks, long_ratio, short_ratio, leverage, pv, 'allocations_from_portfolio')

def set_prices_and_can_trade(context, data):
    all_stocks = set(context.portfolio.positions.keys()).union(
        set(context.adjuster.extra_stocks))
    if context.selector.portfolio:
        all_stocks = all_stocks.union(set(context.selector.portfolio.stock_allocations.keys()))
    if context.target_portfolio:
        all_stocks = all_stocks.union(set(context.target_portfolio.stock_allocations.keys()))
    all_stocks = list(all_stocks)
    context.all_prices = data.current(all_stocks, 'price')
    context.all_can_trade = data.can_trade(all_stocks)

def rebalance(context, data):
    # Initialize
    context.all_prices = None
    if context.target_portfolio:
        set_prices_and_can_trade(context, data)
        current = allocations_from_portfolio(
            context.portfolio, data, context.all_prices, context.account.leverage)
        transactions = compute_transactions(context, current)
        transactions = context.executor.execute_transactions(context, data, current, transactions)
        context.last_transactions = transactions
        if not transactions:
            log_portfolio_allocation("Post rebalance", context, data)
            context.base_portfolio = None
            context.target_portfolio = None
    context.adjuster.record_stats(context, data)

###----------------------------------------
### Quantopian API Implementations

def initialize(context):
    log.info("Initializing")
    set_do_not_order_list(security_lists.leveraged_etf_list)
    context.selector = TrivialStockSelector(context.portfolio.portfolio_value)
    context.executor = StockTransactionExecutor()
    context.adjuster = StockAllocationAdjustment()
    set_context_parameters(context)
    schedule_function(rebalance,
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=1, minutes=30))

def before_trading_start(context, data):
    set_prices_and_can_trade(context, data)
    context.selector.before_trading_start(context, data)
    context.adjuster.before_trading_start(context, data)
    context.selector.select_stocks_if_needed(context, data)
    context.adjuster.adjust_stocks_if_needed(context, data)
